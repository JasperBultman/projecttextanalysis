from collections import Counter
import os
import glob
import nltk
from nltk.metrics import ConfusionMatrix

path_jasper = 'jasper/p**/d****/'
path_lonneke = 'lonneke/p**/d****/'
path_hielke = 'tagged_Hielke/data/p**/d****/'
classes = set('COU CIT NAT PER ORG ANI SPO ENT'.split())
list_jasper = []
list_lonneke = []
list_hielke = []
file_list_jasper = []
file_list_lonneke = []
file_list_hielke = []
interesting_entities_jasper = []
interesting_entities_lonneke = []
interesting_entities_hielke = []

for filename in glob.glob(os.path.join(path_jasper, '*.ent')):  #tags per line to list from jaspers data.
    file_list_jasper.append(filename)
file_list_jasper = sorted(file_list_jasper)
for filename in file_list_jasper:
    for line in open(filename):
        for i in classes:
            if i.lower() in line.lower():
                interesting_entities_jasper.append("yes")
                list_jasper.append(i)
            else:
                interesting_entities_jasper.append("no")
        
for filename in glob.glob(os.path.join(path_lonneke, '*.ent')): #tags per line to list from lonnekes data.
    file_list_lonneke.append(filename)
file_list_lonneke = sorted(file_list_lonneke)
for filename in file_list_lonneke:
    for line in open(filename):
        for i in classes:
            if i.lower() in line.lower():
                interesting_entities_lonneke.append("yes")
                list_lonneke.append(i)
            else:
                interesting_entities_lonneke.append("no")
    
for filename in glob.glob(os.path.join(path_hielke, '*.pos')):  #tags per line to list from hielkes data.
    file_list_hielke.append(filename)
file_list_hielke = sorted(file_list_hielke)
for filename in file_list_hielke:
    for line in open(filename):
        for i in classes:
            if i.lower() in line.lower():
                interesting_entities_hielke.append("yes")
                list_hielke.append(i)
            else:
                interesting_entities_hielke.append("no")

print("Exercise 3: Precision, Recall and F-scores for interesting entities vs non-interesting entities.")
true_positives4 = Counter()
false_negatives4 = Counter()
false_positives4 = Counter()
print("Comparing Jasper with lonneke.")
cm4 = ConfusionMatrix(interesting_entities_jasper[:15288], interesting_entities_lonneke[:15288])
print(cm4)
yes_no = ["yes", "no"]
for i in yes_no:
    for j in yes_no:
        if i == j:
            true_positives4[i] += cm4[i,j]
        else:
            false_negatives4[i] += cm4[i,j]
            false_positives4[j] += cm4[i,j]

avg_fscore4 = 0
fscore_counter4 = 0
print("F-scores per class:")
for i in yes_no:
    if true_positives4[i] == 0:
        fscore4 = 0
    else:
        precision4 = true_positives4[i] / float(true_positives4[i]+false_positives4[i])
        recall4 = true_positives4[i] / float(true_positives4[i]+false_negatives4[i])
        fscore4 = 2 * (precision4 * recall4) / float(precision4 + recall4)
        avg_fscore4 += fscore4
        fscore_counter4 += 1
    print(i, fscore4)
print("Average of the f-scores between Jasper and Lonneke:", avg_fscore4/fscore_counter4)
print()
true_positives5 = Counter()
false_negatives5 = Counter()
false_positives5 = Counter()
print("Comparing Jasper with Hielke.")
cm5 = ConfusionMatrix(interesting_entities_jasper[:15288], interesting_entities_hielke[:15288])
print(cm5)
yes_no = ["yes", "no"]
for i in yes_no:
    for j in yes_no:
        if i == j:
            true_positives5[i] += cm5[i,j]
        else:
            false_negatives5[i] += cm5[i,j]
            false_positives5[j] += cm5[i,j]

avg_fscore5 = 0
fscore_counter5 = 0
print("F-scores per class:")
for i in yes_no:
    if true_positives5[i] == 0:
        fscore5 = 0
    else:
        precision5 = true_positives5[i] / float(true_positives5[i]+false_positives5[i])
        recall5 = true_positives5[i] / float(true_positives5[i]+false_negatives5[i])
        fscore5 = 2 * (precision5 * recall5) / float(precision5 + recall5)
        avg_fscore5 += fscore5
        fscore_counter5 += 1
    print(i, fscore5)
print("Average of the f-scores between Jasper and Hielke:", avg_fscore5/fscore_counter5)
print()
true_positives6 = Counter()
false_negatives6 = Counter()
false_positives6 = Counter()
print("Comparing Lonneke with Hielke.")
cm6 = ConfusionMatrix(interesting_entities_lonneke[:15288], interesting_entities_hielke[:15288])
print(cm6)
yes_no = ["yes", "no"]
for i in yes_no:
    for j in yes_no:
        if i == j:
            true_positives6[i] += cm6[i,j]
        else:
            false_negatives6[i] += cm6[i,j]
            false_positives6[j] += cm6[i,j]

avg_fscore6 = 0
fscore_counter6 = 0
print("F-scores per class:")
for i in yes_no:
    if true_positives6[i] == 0:
        fscore6 = 0
    else:
        precision6 = true_positives6[i] / float(true_positives6[i]+false_positives6[i])
        recall6 = true_positives6[i] / float(true_positives6[i]+false_negatives6[i])
        fscore6 = 2 * (precision6 * recall6) / float(precision6 + recall6)
        avg_fscore6 += fscore6
        fscore_counter6 += 1
    print(i, fscore6)
print("Average of the f-scores between Lonneke and Hielke:", avg_fscore6/fscore_counter6)
print()
true_positives1 = Counter()
false_negatives1 = Counter()
false_positives1 = Counter()
print("Exercise 3: Precision, Recall and F-scores for all entities.")
print("We have used the first 400 elements in the list because the confusion matrix requires the lists to be of equal length.")
print("Comparing Jasper with Lonneke.")
cm1 = ConfusionMatrix(list_jasper[:400], list_lonneke[:400])
print(cm1)
for i in classes:
    for j in classes:
        if i == j:
            true_positives1[i] += cm1[i,j]
        else:
            false_negatives1[i] += cm1[i,j]
            false_positives1[j] += cm1[i,j]


avg_fscore1 = 0
fscore_counter1 = 0
print("F-scores per class:")
for i in sorted(classes):
    if true_positives1[i] == 0:
        fscore1 = 0
    else:
        precision1 = true_positives1[i] / float(true_positives1[i]+false_positives1[i])
        recall1 = true_positives1[i] / float(true_positives1[i]+false_negatives1[i])
        fscore1 = 2 * (precision1 * recall1) / float(precision1 + recall1)
        avg_fscore1 += fscore1
        fscore_counter1 += 1
    print(i, fscore1)
print("Average of the f-scores between Jasper and Lonneke:", avg_fscore1/fscore_counter1)
print()
true_positives2 = Counter()
false_negatives2 = Counter()
false_positives2 = Counter()
print("Comparing Jasper with Hielke.")
cm2 = ConfusionMatrix(list_jasper[:400], list_hielke[:400])
print(cm2)

for i in classes:
    for j in classes:
        if i == j:
            true_positives2[i] += cm2[i,j]
        else:
            false_negatives2[i] += cm2[i,j]
            false_positives2[j] += cm2[i,j]

avg_fscore2 = 0
fscore_counter2 = 0
print("F-scores per class:")
for i in sorted(classes):
    if true_positives2[i] == 0:
        fscore2 = 0
    else:
        precision2 = true_positives2[i] / float(true_positives2[i]+false_positives2[i])
        recall2 = true_positives2[i] / float(true_positives2[i]+false_negatives2[i])
        fscore2 = 2 * (precision2 * recall2) / float(precision2 + recall2)
        avg_fscore2 += fscore2
        fscore_counter2 += 1
    print(i, fscore2)
print("Average of the f-scores between Jasper and Hielke:", avg_fscore2/fscore_counter2)
print()

true_positives3 = Counter()
false_negatives3 = Counter()
false_positives3 = Counter()
print("Comparing Lonneke with Hielke.")
cm3 = ConfusionMatrix(list_lonneke[:400], list_hielke[:400])
print(cm3)

for i in classes:
    for j in classes:
        if i == j:
            true_positives3[i] += cm3[i,j]
        else:
            false_negatives3[i] += cm3[i,j]
            false_positives3[j] += cm3[i,j]

avg_fscore3 = 0
fscore_counter3 = 0
print("F-scores per class:")
for i in sorted(classes):
    if true_positives3[i] == 0:
        fscore3 = 0
    else:
        precision3 = true_positives3[i] / float(true_positives3[i]+false_positives3[i])
        recall3 = true_positives3[i] / float(true_positives3[i]+false_negatives3[i])
        fscore3 = 2 * (precision3 * recall3) / float(precision3 + recall3)
        avg_fscore3 += fscore3
        fscore_counter3 += 1
    print(i, fscore3)
print("Average of the f-scores between Lonneke and Hielke:", avg_fscore3/fscore_counter3)