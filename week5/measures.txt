Exercise 3: Precision, Recall and F-scores for interesting entities vs non-interesting entities.
Comparing Jasper with lonneke.
    |           y |
    |     n     e |
    |     o     s |
----+-------------+
 no |<14517>  312 |
yes |   358  <101>|
----+-------------+
(row = reference; col = test)

F-scores per class:
yes 0.231651376146789
no 0.9774441152706705
Average of the f-scores between Jasper and Lonneke: 0.6045477457087298

Comparing Jasper with Hielke.
    |           y |
    |     n     e |
    |     o     s |
----+-------------+
 no |<14501>  328 |
yes |   352  <107>|
----+-------------+
(row = reference; col = test)

F-scores per class:
yes 0.23937360178970918
no 0.97709049255441
Average of the f-scores between Jasper and Hielke: 0.6082320471720596

Comparing Lonneke with Hielke.
    |           y |
    |     n     e |
    |     o     s |
----+-------------+
 no |<14786>   89 |
yes |    67  <346>|
----+-------------+
(row = reference; col = test)

F-scores per class:
yes 0.8160377358490566
no 0.9947524219590959
Average of the f-scores between Lonneke and Hielke: 0.9053950789040762

Exercise 3: Precision, Recall and F-scores for all entities.
We have used the first 400 elements in the list because the confusion matrix requires the lists to be of equal length.
Comparing Jasper with Lonneke.
    |  A  C  C  E  N  O  P  S |
    |  N  I  O  N  A  R  E  P |
    |  I  T  U  T  T  G  R  O |
----+-------------------------+
ANI | <.> .  .  .  2  1  1  . |
CIT |  . <5> 7  2  1  9  4  . |
COU |  1  8<24> 9  2 30  5  . |
ENT |  2  .  6 <8> 3 17  2  2 |
NAT |  2  2  6  . <1> 9  .  . |
ORG |  3 13 30 20  8<87>11  2 |
PER |  1  2  9  9  2 19 <7> . |
SPO |  .  .  1  1  .  2  . <2>|
----+-------------------------+
(row = reference; col = test)

F-scores per class:
ANI 0
CIT 0.17241379310344826
COU 0.2962962962962963
ENT 0.17977528089887637
NAT 0.05128205128205128
ORG 0.5
PER 0.17721518987341772
SPO 0.3333333333333333
Average of the f-scores between Jasper and Lonneke: 0.24433084925534618

Comparing Jasper with Hielke.
    |  A  C  C  E  N  O  P  S |
    |  N  I  O  N  A  R  E  P |
    |  I  T  U  T  T  G  R  O |
----+-------------------------+
ANI | <3> .  .  .  .  .  1  . |
CIT |  . <6> 4  4  . 13  1  . |
COU |  1 10<10> 6  3 46  3  . |
ENT |  .  2  5 <6> 2 20  5  . |
NAT |  .  2  2  . <4>10  2  . |
ORG |  . 18 38 20  7<65>22  4 |
PER |  .  5  2  3  4 21<14> . |
SPO |  .  .  1  1  2  .  . <2>|
----+-------------------------+
(row = reference; col = test)

F-scores per class:
ANI 0.75
CIT 0.16901408450704225
COU 0.14184397163120568
ENT 0.15
NAT 0.1904761904761905
ORG 0.37249283667621774
PER 0.288659793814433
SPO 0.3333333333333333
Average of the f-scores between Jasper and Hielke: 0.2994775263048028

Comparing Lonneke with Hielke.
    |  A  C  C  E  N  O  P  S |
    |  N  I  O  N  A  R  E  P |
    |  I  T  U  T  T  G  R  O |
----+-------------------------+
ANI | <.> 1  1  .  2  4  1  . |
CIT |  . <3> 4  5  1 16  1  . |
COU |  . 13<21> 5  6 32  6  . |
ENT |  .  7  5 <6> 2 19  7  3 |
NAT |  2  1  3  2 <2> 6  3  . |
ORG |  1 15 22 17  9<86>23  1 |
PER |  1  3  5  5  . 10 <6> . |
SPO |  .  .  1  .  .  2  1 <2>|
----+-------------------------+
(row = reference; col = test)

F-scores per class:
ANI 0
CIT 0.08219178082191782
COU 0.2896551724137931
ENT 0.1348314606741573
NAT 0.0975609756097561
ORG 0.492836676217765
PER 0.15384615384615385
SPO 0.3333333333333333
Average of the f-scores between Lonneke and Hielke: 0.22632222184526804
