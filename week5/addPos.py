#!/usr/bin/python3

import sys
import nltk


def main(argv):
    if len(argv) != 2:
        print("Usage: {} File, File being the file you want to POS-tag".format(argv[0]))
    else:
        text = open(argv[1]).read()
        lines = []
        temp = []
        sents = []
        tags = []
        i = 1
        j = 0
        text = text.split('\n')
        for line in text:
            line = line.split()
            if len(line) == 4:
                lines.append(line)
        for el in lines:
            j += 1
            if el[2][0] == str(i):
                temp.append(el[3])
                if len(lines) == j:
                    sents.append(temp)
            else:
                sents.append(temp)
                i += 1
                temp = []
                temp.append(el[3])
        tagged = [nltk.pos_tag(sent) for sent in sents]
        for k in tagged:
            for item in k:
                tags.append(item[1])
        for l in lines:
            print("{0} {1}".format(" ".join(l), tags[lines.index(l)]))

if __name__ == "__main__":
    main(sys.argv)
