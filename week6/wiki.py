#!/usr/bin/python3
import wikipedia
from nltk.corpus import wordnet
from nltk.wsd import lesk
from nltk import word_tokenize
from nltk import sent_tokenize
import nltk

ny = wikipedia.page("New York")
ny_text = ny.content
ny_sents = sent_tokenize(ny_text)
ny_words = word_tokenize(ny_text)
tagged = []

for sent in ny_sents:
    words = word_tokenize(sent)
    word_pos = nltk.pos_tag(words)
    for word in words:
        for word, pos in word_pos: 
            if pos == "NN" or pos == "NNS" or pos == "NNP" or pos == "NNPS":
                pos = "n"
                tagged.append((list(words),word,pos))   #Build list which contains the sentence a word is in, the word and the postag.

tagged = set(tagged)
for sent, word, pos in list(tagged):
    if len(word) > 2:
        print(lesk(sent, word, pos))


