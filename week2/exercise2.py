#!/usr/bin/python3
# 26 april 2017
# exercise2.py

from nltk.corpus import gutenberg
import nltk
from nltk import ngrams
import collections


def longest_line(tokenize_sents):
    """This prints the longest line in the file"""
    longest_line = ""
    longest_line_len = 0
    for sent in tokenize_sents:
        if len(sent) > longest_line_len:
            longest_line_len = len(sent)
            longest_line = sent
    print("1.A: The longest line:")
    print("Length: {0}\n{1}\n".format(longest_line_len, longest_line))


def shortest_line(tokenize_sents):
    """This prints the shortest line in the file"""
    shortest_line = ""
    # This takes the first sentence as a "base"
    shortest_line_len = len(tokenize_sents[0])
    for sent in tokenize_sents:
        if len(sent) < shortest_line_len:
            shortest_line_len = len(sent)
            shortest_line = sent
    print("1.B: The shortest line:")
    print("Length: {0}\n{1}\n".format(shortest_line_len, shortest_line))


def frequency_dict(tokenize_sents):
    """This prints how often a sentence of a certain length occurs."""
    freq_list = []
    for sent in tokenize_sents:
        freq_list.append(len(sent))

    counter = dict(collections.Counter(freq_list))
    # This gives an ordered dictionary with format linelength, frequency
    counter = dict(collections.OrderedDict(counter))
    print("1.C: The distribution of sentences in terms of length:")
    print("{0:10}{1}".format("Length", "Amount"))
    for key, value in counter.items():
        print("{0:<10}{1}".format(key, value))
    print()  # Prints a new line


def average_length(tokenize_sents):
    """This prints the average length of the sentences."""
    total_length = 0
    sentence_count = 0
    for sent in tokenize_sents:
        total_length = total_length + len(sent)
        sentence_count = sentence_count + 1
    average_sentence_length = total_length/sentence_count
    print("1.D: The average sentence length in the whole document:")
    print(average_sentence_length, "\n")


def character_types(tokenize_sents):
    """Prints sorted list of character occurrences"""
    words = []
    char_types = []
    type_count = 0
    for sent in tokenize_sents:
        # Tokenizes sentences into words
        words.append(nltk.word_tokenize(sent))
    for item in words:
        for word in item:
            for char in word:
                # Adds characters to a list
                char_types.append(char)
    # Counts how often each character appears
    fdist_char = nltk.FreqDist(char_types)
    # Counts how many unique characters there are
    print("2.A: The amount of different character types: \
          {0}".format(len(set(char_types))))
    print("The characters types in the text are as following:")
    for b, f in sorted(fdist_char.items()):
        # Prints sorted list of characters and occurrences
        print(b, f)
    print()


def word_types(tokenize_sents):
    """Prints sorted list of word types"""
    words = []
    word_types = []
    type_count = 0
    for sent in tokenize_sents:
        words.append(nltk.word_tokenize(sent))
    for item in words:
        for word in item:
            word_types.append(word)
    # Counts how many times each word occurs
    fdist_word = nltk.FreqDist(word_types)
    print("2.B: The word types in the text are as following:")
    for b, f in sorted(fdist_word.items()):
        # Print sorted list of occurrences
        print("{0:20}{1}".format(b, f))


def ngram(lst):
    """Counts and prints ngrams"""
    count = collections.Counter(lst).most_common(20)
    for key, value in count:
        print("{0:25}{1}".format(str(key), value))


def ngrams_characters(tokenize_sents):
    """Finds uni-, bi- and trigrams in a text on character-level"""
    characters = []
    for sent in tokenize_sents:
        for word in nltk.word_tokenize(sent):
            for char in word:
                characters.append(char)
    print("2.C: Uni-, bi- and trigrams in characters.")
    print("20 most common unigrams:")
    ngram(characters)
    bi_chars = nltk.bigrams(characters)
    print("20 most common bigrams:")
    ngram(bi_chars)
    tri_chars = nltk.trigrams(characters)
    print("20 most common trigrams:")
    ngram(tri_chars)


def ngrams_words(tokenize_sents):
    """Finds uni-, bi- and trigrams in a text on word-level"""
    words = []
    for sent in tokenize_sents:
        for word in nltk.word_tokenize(sent):
            words.append(word)
    print("2.D: Uni-, bi- and trigrams in words.")
    print("20 most common unigrams:")
    ngram(words)
    bi_tokens = nltk.bigrams(words)
    print("20 most common bigrams:")
    ngram(bi_tokens)
    tri_tokens = nltk.trigrams(words)
    print("20 most common trigrams:")
    ngram(tri_tokens)


def main():
    path = "holmes.txt"
    f = open(path)
    text = f.read()
    tokenize_sents = nltk.sent_tokenize(text)
    # Exercise 2.1a
    longest_line(tokenize_sents)
    # Exercise 2.1b
    shortest_line(tokenize_sents)
    # Exercise 2.1c
    frequency_dict(tokenize_sents)
    # Exercise 2.1d
    average_length(tokenize_sents)
    # Exercise 2.2a
    character_types(tokenize_sents)
    # Exercise 2.2b
    word_types(tokenize_sents)
    # Exercise 2.2c
    ngrams_characters(tokenize_sents)
    # Exercise 2.2d
    ngrams_words(tokenize_sents)


if __name__ == "__main__":
    main()