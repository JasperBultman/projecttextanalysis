def word_types(tokenize_sents):
    """Prints sorted list of word types"""
    words = []
    word_types = []
    type_count = 0
    for sent in tokenize_sents:
        words.append(nltk.word_tokenize(sent))    #Tokenizes the sentences in words
    for item in words:
        for word in item:      
           word_types.append(word)           #Adds word to a list 
    fdist_word = nltk.FreqDist(word_types)        #Counts how many times each word occurs 
    print("E: The word types in the text are as following:\n")
    for b, f in sorted(fdist_word.items()):
        print(b, f)                               #Print sorted list of the word type occurences

