#!/usr/bin/bash
# Jasper Bultman
# 26 april 2017
# exercise1.py

import nltk
from nltk.corpus import gutenberg

def exercise1():
    print(gutenberg.fileids()) # shows which files are in the gutenberg corpus.
    chestertonText = gutenberg.raw("chesterton-brown.txt")
    print(chestertonText[:300]) #prints the first 300 characters in the string of chesterton-brown.txt.
    chestertonWords = gutenberg.words("chesterton-brown.txt")
    print(chestertonWords[:20]) #prints the first 20 words/tokens of the chesterton-brown textfile.
    chestertonSents = gutenberg.sents("chesterton-brown.txt")
    print(chestertonSents[:5]) #prints the first 5 sentences in the chesterton-brown textfile.

exercise1()

def holmes():
    path = "holmes.txt"
    f = open(path)
    raw = f.read()
    f.close()
    print(raw[:100])    # prints first 100 characters in the holmes textfile.
    sents = nltk.sent_tokenize(raw) # tokenizes the holmes textfile's sentences.
    print(sents[30:32]) # prints the 30th sentence till the 32nd sentence.
    tokens = [] # tokenizes the sentences into words
    for sent in sents:
        tokens += nltk.word_tokenize(sent) 
    print(tokens[:10]) # prints the first 10 words
    example_sents = sents[0:10]
    bigrams = nltk.bigrams(example_sents)   # makes bigrams out of the example sentences
    trigrams = nltk.trigrams(example_sents) # makes trigrams out of the example sentences
    fdist_bigrams = nltk.FreqDist(bigrams)  # counts the frequency of the bigrams
    fdist_trigrams = nltk.FreqDist(trigrams) # counts the frequency of the trigrams
    for b, f in fdist_bigrams.items():
        print(b, f)                     # prints the bigram and its frequency
    for t, f in fdist_trigrams.items():
        print(t, f)                     # prints the trigram and its frequency

    
holmes()
