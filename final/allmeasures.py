from collections import Counter
import os
import glob
import nltk
from nltk.metrics import ConfusionMatrix

path_our_output = 'output/p**/d****'
path_development = 'development/p**/d****'
classes = ["COU","CIT","NAT","PER","ORG","ANI","SPO","ENT"]
list_jasper = []
list_lonneke = []
list_hielke = []
file_list_jasper = []
file_list_lonneke = []
file_list_hielke = []
interesting_entities_jasper = []
interesting_entities_lonneke = []
interesting_entities_hielke = []

for filename in glob.glob(os.path.join(path_our_output, '*.ent')):  #tags per line to list from jaspers data.
    file_list_jasper.append(filename)
for filename in file_list_jasper:
    text = open(filename)
    for line in text:
        for i in classes:
            if i.lower() in line.lower():
                interesting_entities_jasper.append("link")
                list_jasper.append(i)
            else:
                interesting_entities_jasper.append("no_link")
        
for filename in glob.glob(os.path.join(path_development, '*.ent')): #tags per line to list from lonnekes data.
    file_list_lonneke.append(filename)
for filename in file_list_lonneke:
    text = open(filename)
    for line in text:
        for i in classes:
            if i.lower() in line.lower():
                interesting_entities_lonneke.append("link")
                list_lonneke.append(i)
            else:
                interesting_entities_lonneke.append("no_link")
  
true_positives4 = Counter()
false_negatives4 = Counter()
false_positives4 = Counter()
cm4 = ConfusionMatrix(interesting_entities_jasper[:1000], interesting_entities_lonneke[:1000])
print(cm4)
yes_no = ["link", "no_link"]

for i in yes_no:
    for j in yes_no:
        if i == j:
            true_positives4[i] += cm4[i,j]
        else:
            false_negatives4[i] += cm4[i,j]
            false_positives4[j] += cm4[i,j]

avg_fscore4 = 0
fscore_counter4 = 0

print()
print("Precision, Recall and F-scores for all entities.")

cm1 = ConfusionMatrix(list_jasper[:1000], list_lonneke[:1000])
print(cm1)
true_positives1 = Counter()
false_negatives1 = Counter()
false_positives1 = Counter()

for i in classes:
    for j in classes:
        if i == j:
            true_positives1[i] += cm1[i,j]
        else:
            false_negatives1[i] += cm1[i,j]
            false_positives1[j] += cm1[i,j]


avg_fscore1 = 0
fscore_counter1 = 0
print("     Recall                  Precision              F-scores per class:")
for i in sorted(classes):
    if true_positives1[i] == 0:
        fscore1 = 0
    else:
        precision1 = true_positives1[i] / float(true_positives1[i]+false_positives1[i])
        recall1 = true_positives1[i] / float(true_positives1[i]+false_negatives1[i])
        fscore1 = 2 * (precision1 * recall1) / float(precision1 + recall1)
        avg_fscore1 += fscore1
        fscore_counter1 += 1

    print(i,"|", recall1,"|", precision1,"|", fscore1)
print("Recall: ", recall1)
print("Precision: ", precision1)
print("Average of the f-scores of all entities:", avg_fscore1/fscore_counter1)

