#!/usr/bin/python3

import sys
import nltk
from nltk.tag import StanfordNERTagger
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import wordnet
from geotext import GeoText


def addPos(text):
    """Takes a text, and returns the lines
       POS-tagged in a list."""
    text = text.split('\n')
    taggedSents = []
    lines = []
    temp = []
    sents = []
    tags = []
    i = 1
    j = 0
    for line in text:
        line = line.split()
        if len(line) == 4:
            lines.append(line)
    for el in lines:
        j += 1
        if el[2][0] == str(i):
            temp.append(el[3])
            if len(lines) == j:
                sents.append(temp)
        else:
            sents.append(temp)
            i += 1
            temp = []
            temp.append(el[3])
    tagged = [nltk.pos_tag(sent) for sent in sents]
    for k in tagged:
        for item in k:
            tags.append(item[1])
    for l in lines:
        item = "{0} {1}".format(" ".join(l), tags[lines.index(l)])
        taggedSents.append(item)
    return taggedSents


def make_sents(text):
    i = 1
    j = 0
    temp = []
    sents = []
    for line in text:
        line = line.split()
        j += 1
        if line[2][0] == str(i):
            temp.append(line[3])
            if len(text) == j:
                sents.append(temp)
        else:
            sents.append(temp)
            i += 1
            temp = []
            temp.append(line[3])
    return sents


def new_sents(tagged):
    lst = []
    temp = []
    for sent in tagged:
        for word in sent:
            temp.append(word[0])
        lst.append(temp)
        temp = []
    return lst

def NER_Tagger(sentences):
    model = 'stanford-ner-2016-10-31/classifiers/english.conll.4class.distsim.crf.ser.gz'
    java = 'stanford-ner-2016-10-31/stanford-ner.jar'
    st = StanfordNERTagger(model, java, encoding='utf8')
    ner_tagged = [st.tag(sent) for sent in sentences]
    return ner_tagged

def entity_tagger(ner_tagged, text):
    "Install the GeoText library for this. *sudo pip3 install geotext"
    entity_tagged = []
    newList = []
    countries = open("countries.txt").readlines()
    country_list = [country.strip() for country in countries]
    wordList = []
    count = 0
    for entity_set in ner_tagged:
        templist = []
        lastEnt = ""
        for word, entity in entity_set:
            syn = wordnet.synsets(word)
            place = GeoText(word)
            link = ""
            wordList.append(word)
            surrounding = wordList.index(word)
            if entity == "LOCATION": 
                entity = "NAT"               
                for s in syn:
                    if len(place.cities) > 0:
                        entity = "CIT"
                    elif "country" in s.definition():
                        entity = "COU"
                    elif wordList[surrounding:surrounding+count] or wordList[surrounding-count:surrounding] in country_list:
                        entity = "COU"
                    else:
                        entity = "NAT"
                link = "http://en.wikipedia.org/wiki/" + word
            elif entity == "ORGANIZATION":
                entity = "ORG"
            elif entity == "PERSON":
                entity = "PER"
            elif entity == "MISC":
                entity = "ORG"
            elif entity == "O":
                synset = wordnet.synsets(word)
                for s in synset:
                    hypernyms = s.hypernyms()
                    for hypernym in hypernyms:
                        next_hypernyms = hypernym.hypernyms()
                        if "animal" in hypernym.name():
                            entity = "ANI"
                        elif "sport" in hypernym.name():
                            entity = "SPO"
                        elif "book" in hypernym.name():
                            entity = "ENT"
                        elif "magazine" in hypernym.name():
                            entity = "ENT"
                        elif "film" in hypernym.name():
                            entity = "ENT"
                        elif "song" in hypernym.name():
                            entity = "ENT"
                        elif "concert" in hypernym.name():
                            entity = "ENT"
                        else:
                            for next_hypernym in next_hypernyms:
                                last_hypernyms = next_hypernym.hypernyms()
                                if "animal" in next_hypernym.name():
                                    entity = "ANI"
                                elif "sport" in next_hypernym.name():
                                    entity = "SPO"
                                elif "book" in next_hypernym.name():
                                    entity = "ENT"
                                elif "magazine" in next_hypernym.name():
                                    entity = "ENT"
                                elif "film" in next_hypernym.name():
                                    entity = "ENT"
                                elif "song" in next_hypernym.name():
                                    entity = "ENT"
                                elif "concert" in next_hypernym.name():
                                    entity = "ENT"
                                else:
                                    for last_hypernym in last_hypernyms:
                                        if "animal" in last_hypernym.name():
                                            entity = "ANI"
                                        elif "sport" in last_hypernym.name():
                                            entity = "SPO"  
                                        elif "book" in last_hypernym.name():
                                            entity = "ENT"
                                        elif "magazine" in last_hypernym.name():
                                            entity = "ENT"  
                                        elif "film" in last_hypernym.name():
                                            entity = "ENT"  
                                        elif "song" in last_hypernym.name():
                                            entity = "ENT" 
                                        elif "concert" in last_hypernym.name():
                                            entity = "ENT"
            else:
                entity = "O"
            newList.append((word, entity))
        count += 1

    ind = 0
    finalList = []
    for el in newList:
        if ind == 0:
            if newList[ind+1][1] == "O" and el[1] != "O":
                link = "http://en.wikipedia.org/wiki/" + el[0]
                finalList.append((el[0], el[1], link))
            elif el[1] != "O":
                templist.append(el[0])
            else:
                finalList.append((el[0]))
        elif ind == len(newList) - 1:
            if newList[ind-1][1] == "O" and el[1] != "O":
                link = "http://en.wikipedia.org/wiki/" + el[0]
                finalList.append((el[0], el[1], link))
        else:
            if newList[ind+1][1] == "O" and newList[ind-1][1] == "O" and el[1] != "O":
                link = "http://en.wikipedia.org/wiki/" + el[0]
                finalList.append((el[0], el[1], link))
            elif el[1] != "O":
                templist.append(el[0])
                if newList[ind+1][1] != el[1]:
                    linkword = "_".join(templist)
                    for item in templist:
                        link = "http://en.wikipedia.org/wiki/" + linkword
                        finalList.append((item, el[1], link))
                    templist = []
            else:
                finalList.append((el[0]))
        ind += 1
    temp = []
    entityList = []
    for line in text:
        temp.append(line)
    i = 0
    for thing in finalList:
        if len(thing) != 3:
            item = temp[i]
        else:
            item = temp[i] + " " + thing[1] + " " + thing[2]
        entityList.append(item)
        i += 1
    for el in entityList:
        print(el)


def main(argv):
    if len(argv) != 2:
        print("Usage: {} File > en.tok.off.pos.ent, File being the file you want to wifify.")
    else:
        text = open(argv[1]).read()
        text = addPos(text)
        sentences = make_sents(text)
        tagged = [nltk.pos_tag(sent) for sent in sentences]
        sentences = new_sents(tagged)
        chunked = [nltk.ne_chunk(taggedSent) for taggedSent in tagged]
        ner_tagged = NER_Tagger(sentences)
        tagged_word = entity_tagger(ner_tagged, text)

if __name__ == "__main__":
    main(sys.argv)
