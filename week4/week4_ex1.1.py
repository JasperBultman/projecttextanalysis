#!/usr/bin/python3

import nltk
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import wordnet


def hypernymOf(synset1, synset2):
    """ Returns True if synset2 is a hypernym of synset1, or if they are the same synset.mReturns False otherwise."""
    if  synset1 == synset2:
        return True
    for hypernym in synset1.hypernyms():
        if synset2 == hypernym:
            return True
        if hypernymOf(hypernym, synset2):
            return True
    return False

def relations(tokenize):
    relativeSynsets = wordnet.synsets("relative", pos="n")
    illnesSynsets = wordnet.synsets("illness", pos="n")
    scienceSynsets = wordnet.synsets("science", pos="n")
    lemmatizer = WordNetLemmatizer()
    lemma_list = []
    for word in tokenize:
        lemma = lemmatizer.lemmatize(word, "n")
        lemma_list.append((word,lemma))
    synset_list = []
    for word, lemma in lemma_list:
        synset = wordnet.synsets(lemma, pos="n")
        synset_list.append((word, synset))
    relative = relativeSynsets[0]
    word_count_relative = 0
    relative_list = []
    illness = illnesSynsets[0]
    word_count_illness = 0
    illness_list = []
    science1 = scienceSynsets[0]
    science2 = scienceSynsets[1]
    word_count_science = 0
    science_list = []
    for word, synsets in synset_list:
        if synsets != "[]":
            for synset in synsets:
                if hypernymOf(synset, relative):
                    relative_list.append(word)
                    word_count_relative += 1
                if hypernymOf(synset, illness):
                    illness_list.append(word)
                    word_count_illness += 1
                if hypernymOf(synset, science1) or hypernymOf(synset, science2):
                    science_list.append(word)
                    word_count_science += 1
    print(relative_list, word_count_relative) #1.1A
    print(illness_list, word_count_illness)   #1.1B
    print(science_list, word_count_science)   #1.1C
    
    


def main():
    path= "ada_lovelace.txt"
    f = open(path)
    text = f.read()
    tokenize = nltk.wordpunct_tokenize(text)
    relations(tokenize) # 1.1

if __name__ == "__main__":
    main()
