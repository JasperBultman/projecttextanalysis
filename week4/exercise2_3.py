#!/usr/bin/python3

from nltk.tag import StanfordNERTagger
from collections import Counter
import nltk


def main():
    model = 'stanford-ner-2016-10-31/classifiers/english.all.3class.distsim.crf.ser.gz'
    java = 'stanford-ner-2016-10-31/stanford-ner.jar'
    text = open('ada_lovelace.txt').read()
    tagger = StanfordNERTagger(model, java, encoding='utf8')
    sents = nltk.sent_tokenize(text)
    temptoken = []
    tokens = []
    bos = []
    for sent in sents:
        temptoken += nltk.word_tokenize(sent)
        tokens.append(temptoken)
        temptoken = []
    for item in tokens:
        taggedItem = nltk.pos_tag(item)
        boompje = nltk.ne_chunk(taggedItem)
        bos.append(boompje)
    for el in bos:
        for i in el:
            if type(i) != tuple:
                print(i)


