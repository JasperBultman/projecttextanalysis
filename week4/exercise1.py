#!/usr/bin/python3

import nltk
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import wordnet


def hypernymOf(synset1, synset2):
    """Checks whether synset2 is a hypernym of synset1 \
       or if they're the same synset."""
    if synset1 == synset2:
        return True
    for hypernym in synset1.hypernyms():
        if synset2 == hypernym:
            return True
        if hypernymOf(hypernym, synset2):
            return True
    return False

def hypernymEntity(word, synset):
    synset = synset[0].hypernyms()
    if len(synset) < 1:
        return False
    if synset[0].hypernyms()[0].hypernyms()[0] == synset[0].root_hypernyms()[0]:
        print("{0:20}{1}".format(word, synset[0]))
    else:
        hypernymEntity(word, synset)

def return_synsets(tokens):
    lemmatizer = WordNetLemmatizer()
    pos_tags = nltk.pos_tag(tokens)
    lemmas = [(item[0], lemmatizer.lemmatize(item[0], wordnet.NOUN))
              for item in pos_tags if item[1][:2] == "NN"]
    synsets = [(word, wordnet.synsets(lemma, pos="n"))
               for word, lemma in lemmas]
    return synsets

def part1(tokens):
    relative = wordnet.synsets("relative", pos="n")
    illness = wordnet.synsets("illness", pos="n")
    science = wordnet.synsets("science", pos="n")

    synsets = return_synsets(tokens)

    # Define empty lists
    rList = []
    iList = []
    sList = []

    # Define counters
    rCount = 0
    iCount = 0
    sCount = 0

    for word, synset in synsets:
        for i in synset:
            if hypernymOf(i, relative[0]):
                rList.append(word)
                rCount += 1
            if hypernymOf(i, illness[0]):
                iList.append(word)
                iCount += 1
            if hypernymOf(i, science[0]) or hypernymOf(i, science[1]):
                sList.append(word)
                sCount += 1
    print("Exercise 1.1")
    # 1.1A
    print("Number of nouns referring to a relative: {0}.".format(rCount))
    print(rList, "\n")
    # 1.1B
    print("Number of nouns referring to an illness: {0}.".format(iCount))
    print(iList, "\n")
    # 1.1C
    print("Number of nouns referring to a science: {0}.".format(sCount))
    print(sList, "\n")

def part2(tokens):
    print("Exercise 1.2")
    synsets = return_synsets(tokens)
    count = 0
    hypList = []
    lst = [el for el in synsets if len(el[1]) >= 1]
    print("Nouns and their synsets:")
    for el in lst:
        hypernymEntity(el[0], el[1])


def getMaxSim(synsets1, synsets2):
    maxSim = None
    for s1 in synsets1:
        for s2 in synsets2:
            sim = s1.lch_similarity(s2)
            if maxSim == None or maxSim < sim:
                maxSim = sim
    return maxSim

def part3():
    carSynsets = wordnet.synsets("car", pos="n")
    automobileSynsets = wordnet.synsets("automobile", pos="n")
    coastSynsets = wordnet.synsets("coast", pos="n")
    shoreSynsets = wordnet.synsets("shore", pos="n")
    foodSynsets = wordnet.synsets("food", pos="n")
    fruitSynsets = wordnet.synsets("fruit", pos="n")
    journeySynsets = wordnet.synsets("journey", pos="n")
    monkSynsets = wordnet.synsets("monk", pos="n")
    slaveSynsets = wordnet.synsets("slave", pos="n")
    moonSynsets = wordnet.synsets("moon", pos="n")
    stringSynsets = wordnet.synsets("string", pos="n")
    
    print()
    print("Exercise 1.3")
    print("car<>automobile:", getMaxSim(carSynsets, automobileSynsets))
    print("coast<>shore:", getMaxSim(coastSynsets, shoreSynsets))
    print("food<>fruit:", getMaxSim(foodSynsets, fruitSynsets))
    print("journey<>car:", getMaxSim(journeySynsets, carSynsets))
    print("monk<>slave:", getMaxSim(monkSynsets, slaveSynsets))
    print("moon<>string:", getMaxSim(moonSynsets, stringSynsets))

    print("There is quite a difference between these values and the values of Miller and Charles. This is because of the fact that WordNet defines the simularity based on the real meaning of the word. In the experiment of Miller and Charles they asked people. When you hear the word coast and shore you will think this is very close because it does look the same when you see it in real life, but a computer only has the real meaning and not this visualization which is why it has such different outputs.")


def main():
    text = open("ada_lovelace.txt").read()
    tokens = nltk.wordpunct_tokenize(text)
    part1(tokens)
    part2(tokens)
    part3()

if __name__ == "__main__":
    main()
