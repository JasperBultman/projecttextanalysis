#!/usr/bin/python3

from nltk.tag import StanfordNERTagger
from collections import Counter
import nltk


def exercise2_12():
    model = 'stanford-ner-2016-10-31/classifiers/english.conll.4class.distsim.crf.ser.gz'
    java = 'stanford-ner-2016-10-31/stanford-ner.jar'
    f = open('ada_lovelace.txt')
    text = f.read()
    st = StanfordNERTagger(model, java, encoding='utf8')
    st = st.tag(text.split())
    tag_list = []
    for el in st:
        if el[1] != 'O':
            print(el)
            tag_list.append(el)
    c = Counter()
    for el in st:
        if el[1] != 'O':
            c.update([el[1]])
    print("2.1\nThe found tags by the Stanford Tagger are: {0}: \nThese are the tagged words: \n{1}\n\n The tagger provides a lot of good information but sometimes confuses a person with a organisation eventhough it already tagged it as a person earlier.\n".format(c.most_common(), (tag_list)))
    print("2.2\nThe other models add other kinds of tags. One model has the 'misc' tag which tries to find words that don't fit the other catogories and the other one can also tag a date. If you know what kind of words you want to tag this information is uselful to know which model to use when tagging a text. Since we need to tag Person, Organisation and Location the first tagger model is the best one since it only has those 3.")

def exercise2_3():
    print("Exercise 2.3:")
    model = 'stanford-ner-2016-10-31/classifiers/english.conll.4class.distsim.crf.ser.gz'
    java = 'stanford-ner-2016-10-31/stanford-ner.jar'
    text = open('ada_lovelace.txt').read()
    tagger = StanfordNERTagger(model, java, encoding='utf8')
    sents = nltk.sent_tokenize(text)
    temptoken = []
    tokens = []
    bos = []
    for sent in sents:
        temptoken += nltk.word_tokenize(sent)
        tokens.append(temptoken)
        temptoken = []
    for item in tokens:
        taggedItem = nltk.pos_tag(item)
        boompje = nltk.ne_chunk(taggedItem)
        bos.append(boompje)
    for el in bos:
        for i in el:
            if type(i) != tuple:
                print(i)


def main():
    exercise2_12()
    exercise2_3()

if __name__ == "__main__":
    main()
