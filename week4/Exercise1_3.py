#!/usr/bin/python3
from nltk.corpus import wordnet


def getMaxSim(synsets1, synsets2):
    maxSim = None
    for s1 in synsets1:
        for s2 in synsets2:
            sim = s1.lch_similarity(s2)
            if maxSim == None or maxSim < sim:
                maxSim = sim
    return maxSim

def part3():
    carSynsets = wordnet.synsets("car", pos="n")
    automobileSynsets = wordnet.synsets("automobile", pos="n")
    coastSynsets = wordnet.synsets("coast", pos="n")
    shoreSynsets = wordnet.synsets("shore", pos="n")
    foodSynsets = wordnet.synsets("food", pos="n")
    fruitSynsets = wordnet.synsets("fruit", pos="n")
    journeySynsets = wordnet.synsets("journey", pos="n")
    monkSynsets = wordnet.synsets("monk", pos="n")
    slaveSynsets = wordnet.synsets("slave", pos="n")
    moonSynsets = wordnet.synsets("moon", pos="n")
    stringSynsets = wordnet.synsets("string", pos="n")
       
    print("car<>automobile:", getMaxSim(carSynsets, automobileSynsets))
    print("coast<>shore:", getMaxSim(coastSynsets, shoreSynsets))
    print("food<>fruit:", getMaxSim(foodSynsets, fruitSynsets))
    print("journey<>car:", getMaxSim(journeySynsets, carSynsets))
    print("monk<>slave:", getMaxSim(monkSynsets, slaveSynsets))
    print("moon<>string:", getMaxSim(moonSynsets, stringSynsets))

    print("There is quite a difference between these values and the values of Miller and Charles. This is because of the fact that WordNet defines the simularity based on the real meaning of the word. In the experiment of Miller and Charles they asked people. When you hear the word coast and shore you will think this is very close because it does look the same when you see it in real life, but a computer only has the real meaning and not this visualization which is why it has such different outputs.")
