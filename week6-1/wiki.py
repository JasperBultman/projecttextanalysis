#!/usr/bin/python3
import wikipedia
from nltk.corpus import wordnet
from nltk.wsd import lesk
from nltk import word_tokenize
from nltk import sent_tokenize
import nltk
from collections import Counter

pagein = input("Enter wikipedia page here: ")
page = wikipedia.page(str(pagein))
content = page.content
syns = []
poly = []

for sent in nltk.sent_tokenize(content):
    sent = nltk.word_tokenize(sent)
    tagged = nltk.pos_tag(sent)
    for word, tag in tagged:
        if tag[:2] == "NN":
            pos = "n"
            if len(wordnet.synsets(word)) == 1:
                syns.append((word, wordnet.synsets(word)[0]))
            elif len(wordnet.synsets(word)) == 0:
                pass
            else:
                syns.append((word, lesk(sent, word, pos)))

for el in syns:
    print(el)
