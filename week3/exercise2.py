#!/usr/bin/python3
#
#
#

import nltk
import sys
import collections
from random import randrange
from nltk.collocations import *

def exercise2_2():
    words = 0
    sents = 0
    br_tw = nltk.corpus.brown.tagged_words(categories='adventure')
    br_ts = nltk.corpus.brown.tagged_sents(categories='adventure')
    br_sent = nltk.corpus.brown.sents(categories='adventure')
    br_word = nltk.corpus.brown.words(categories='adventure')

    # 2A
    for el in br_tw:
        words += 1
    for el in br_ts:
        sents += 1
    print("2A: There are {0} words and {1} sentences.\n"
          .format(words, sents))

    # 2B
    word50 = br_tw[50]
    word75 = br_tw[75]
    print("2B: The 50th word is: {0}\nThe 75th word is: {1}\n"
          .format(word50, word75))

    # 2C, 2D, 2E
    c_tag = collections.Counter()
    c_word = collections.Counter()
    tag_count = 0
    for el in br_tw:
            c_tag.update([el[1]])
            c_word.update([el[0]])
    for count in c_tag:
        tag_count += 1
    print("2C: There are {0} different tags.\n".format(tag_count))

    print("2D: The top 15 words and their counts are\n{0}\n"
          .format(c_word.most_common(15)))

    print("2E: The top 15 tags and their counts are\n{0}\n"
          .format(c_tag.most_common(15)))

    # 2F
    sent_20 = br_ts[20]
    sent_40 = br_ts[40]
    c_tag = collections.Counter()
    for el in sent_20:
        c_tag.update([el[1]])
    print("2F: The most frequent tag in sentence 20 is: {0}"
          .format(c_tag.most_common(1)))
    for el in sent_40:
        c_tag.update([el[1]])
    print("The most frequent tag in sentence 40 is: {0}\n"
          .format(c_tag.most_common(1)))

    # 2G
    adv_list = collections.Counter()
    for el in br_tw:
        if el[1] == "RB":
            adv_list.update(el)
    del adv_list["RB"]
    print("2G: The most common adverb is {0}\n".format(adv_list.most_common(1)))

    # 2H
    adj_list = collections.Counter()
    for el in br_tw:
        if "JJ" in el[1]:
            adj_list.update(el)
    ignore = ['JJ', 'JJS', 'JJR']
    for word in ignore:
        if word in adj_list:
            del adj_list[word]
    print("2H: The most common adjective is {0}\n"
          .format(adj_list.most_common(1)))

    # 2I, 2J
    so_list = collections.Counter()
    for el in br_tw:
        if el[0] == "so":
            so_list.update(el)
    print("2I: The word so can be used as a QL (Qualifier), CS (Subordinating Conjunction) and RB (Adverb)\n")
    print("2J: The distribution of the word so over the pos tags is {0}\n"
          .format(so_list))

    # 2K
    so_sents = []
    so_sents_pre = []
    so_sents_pos = []
    for el in br_ts:
        for sent in el:
            if sent[0] == "so" and sent[1] == "QL":
                so_sents.append(el)
    randomnum = randrange(0, len(so_sents))
    useSent = so_sents[randomnum]
    wordList = []
    for el in useSent:
        wordList.append(el[0])
    print("2K: The word 'so' used as a QL:\n{0}".format(" ".join(wordList)))
    so_sents = []
    for el in br_ts:
        for sent in el:
            if sent[0] == "so" and sent[1] == "CS":
                so_sents.append(el)

    randomnum = randrange(0, len(so_sents))
    useSent = so_sents[randomnum]
    wordList = []
    for el in useSent:
        wordList.append(el[0])
    print("The word 'so' used as a CS:\n{0}".format(" ".join(wordList)))
    so_sents = []
    for el in br_ts:
        for sent in el:
            if sent[0] == "so" and sent[1] == "RB":
                so_sents.append(el)
    randomnum = randrange(0, len(so_sents))
    useSent = so_sents[randomnum]
    wordList = []
    for el in useSent:
        wordList.append(el[0])
    print("The word so used as an RB:\n{0}\n".format(" ".join(wordList)))

    # 2L
    soList = []
    preListTemp = []
    folListTemp = []
    for elin, thing in enumerate(br_tw):
        if thing[0] == "so":
            try:
                if elin == 0:
                    raise IndexError
                temp = []
                before = br_tw[elin - 1]
                after = br_tw[elin + 1]
                temp.append(before)
                temp.append(thing)
                temp.append(after)
                soList.append(temp)
                preListTemp.append([before[1]])
                folListTemp.append([after[1]])
            except IndexError:
                pass
    preList = [el[0] for el in preListTemp]
    folList = [el[0] for el in folListTemp]
    pre_c = collections.Counter()
    for el in preList:
        pre_c.update(preList)
    fol_c = collections.Counter()
    for el in folList:
        fol_c.update(folList)
    print("2L: The most common POS preceding so is: {0}.".format(pre_c.most_common(1)))
    print("The most common POS following 'so' is: {0}.\n".format(fol_c.most_common(1)))


def exercise2_34():
    wordList = []
    f = open("holmes.txt")
    text = f.read()
    sents = nltk.sent_tokenize(text)
    for line in sents:
        line = nltk.word_tokenize(line)
        # Exercise 2.3
        line = nltk.pos_tag(line)
        for word in line:
            wordList.append(word)
    # Exercise 2.4
    tagList = [el[1] for el in wordList]
    bigrams = nltk.collocations.BigramAssocMeasures()
    finder = BigramCollocationFinder.from_words(tagList)
    print("The five most common collocations using chi squared:")
    print(finder.score_ngrams(bigrams.chi_sq)[:5])
    print("The five most common collocations using raw frequency:")
    print(finder.score_ngrams(bigrams.raw_freq)[:5])
    print()
    print("They differ very much from one another. It mostly looks confusing.")
    print("It could be interesting to research why certain collocations go together.")


def main():
    exercise2_2()
    exercise2_34()


if __name__ == "__main__":
    main()
