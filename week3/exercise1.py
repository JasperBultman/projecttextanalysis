#!/usr/bin/python3
# 26 april 2017
# week2_ex1.py

import nltk
from nltk import ngrams
from nltk.collocations import *
from nltk.metrics.spearman import *


def collocation(tokenize_words, score_list1):
    """Prints the most likely collocations using PMI"""
    print("1.A The 20 most likely collocations using PMI:")
    print("It only finds the collocations of words that \
          occur exactly once, because they get the highest score.\n")
    bigram_measures = nltk.collocations.BigramAssocMeasures()
    finder = BigramCollocationFinder.from_words(tokenize_words)
    counter = 0
    for i, k in finder.score_ngrams(bigram_measures.pmi):
        counter += 1
        if counter <= 20:
            print(i, k)
        score_list1.append((i, k))


def chi_sq(tokenize_words, score_list2):
    """Prints the 20 most likely collocations using X^2"""
    print("\n1.B The 20 most likely collocations using X^2:")
    print("It only finds the collocations of words that \
           occur exactly once, because they get the highest score.\n")
    bigram_measures = nltk.collocations.BigramAssocMeasures()
    finder = BigramCollocationFinder.from_words(tokenize_words)
    counter = 0
    for i, k in finder.score_ngrams(bigram_measures.chi_sq):
        counter += 1
        if counter <= 20:
            print(i, k)
        score_list2.append((i, k))


def raw_freq(tokenize_words, score_list3):
    """Prints the 20 most likely collocations using a frequency counter"""
    print("\n1.C The 20 most likely collocations using \
          a raw frequency counter:\n")
    bigram_measures = nltk.collocations.BigramAssocMeasures()
    finder = BigramCollocationFinder.from_words(tokenize_words)
    counter1 = 0
    for i, k in finder.score_ngrams(bigram_measures.raw_freq):
        counter1 += 1
        if counter1 <= 20:
            print(i, k)
        score_list3.append((i, k))
    print("\nThere is a big difference in rankings between \
          a raw frequency counter and the pmi and chi-square \
          measures, the entire top 10 is different, but that \
          is also because the pmi and chi-square measures in this \
          case are flawed. Therefore I cannot give an explanation for \
          the difference in rankings. The results are not correct.")


def spearman(score_list1, score_list2, score_list3):
    print("\n1.D The spearman correlation of the \
          different association measures used:")
    results_list = []
    count1 = 0
    for i in score_list1:
        count1 += 1
        if count1 <= 2:
            results_list.append(i)
    count2 = 0
    for i in score_list2:
        count2 += 1
        if count2 <= 2:
            results_list.append(i)
    count3 = 0
    for i in score_list3:
        count3 += 1
        if count3 <= 2:
            results_list.append(i)
    spearman_list1 = list(ranks_from_scores(score_list1[0:5]))
    spearman_list2 = list(ranks_from_scores(score_list2[0:5]))
    spearman_list3 = list(ranks_from_scores(score_list3[0:5]))
    spearman_results1 = spearman_correlation(ranks_from_sequence(spearman_list1), ranks_from_sequence(spearman_list2))
    print("The spearman correlation coefficient result of pmi and chi-square:")
    print(spearman_results1, "\n")
    spearman_results2 = spearman_correlation(ranks_from_sequence(spearman_list1), ranks_from_sequence(spearman_list3))
    print("\nThe spearman correlation coefficient result of pmi and raw-frequency:")
    print(spearman_results2, "\n")
    spearman_results3 = spearman_correlation(ranks_from_sequence(spearman_list2), ranks_from_sequence(spearman_list3))
    print("\nThe spearman correlation coefficient result of chi-square and raw-frequency:")
    print(spearman_results3)
    print("The correlation is all either 0 or 1 because the top 20 of the pmi measure and the chi-square measure have the exact same ranking.")


def main():
    path = "holmes.txt"
    f = open(path)
    text = f.read()
    score_list1 = []
    score_list2 = []
    score_list3 = []
    tokenize_words = nltk.wordpunct_tokenize(text)
    collocation(tokenize_words, score_list1)            # 1A
    chi_sq(tokenize_words, score_list2)                 # 1B
    raw_freq(tokenize_words, score_list3)               # 1C
    spearman(score_list1, score_list2, score_list3)     # 1D (extra)
if __name__ == "__main__":
    main()
